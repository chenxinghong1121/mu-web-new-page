import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Header
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './header/login/login.component';
import { RegisterComponent } from './header/register/register.component';
import { RepasswordComponent } from './header/repassword/repassword.component';

// Body
import { BodyComponent } from './body/body.component';
import { ClientManagerComponent } from './body/client-manager/client-manager.component';
import { ProjectManagerComponent } from './body/project-manager/project-manager.component';
import { FileManagerComponent } from './body/file-manager/file-manager.component';
import { FinanceManagerComponent } from './body/finance-manager/finance-manager.component';
import { PersonnelManagerComponent } from './body/personnel-manager/personnel-manager.component';
import { ClientInfoComponent } from './body/client-manager/client-info/client-info.component';
import { ClientServeComponent } from './body/client-manager/client-serve/client-serve.component';
import { ClientReserveComponent } from './body/client-manager/client-reserve/client-reserve.component';
import { ClientDevelopComponent } from './body/client-manager/client-develop/client-develop.component';
import { ClientTradeComponent } from './body/client-manager/client-trade/client-trade.component';
import { ClientAnalyzeComponent } from './body/client-manager/client-analyze/client-analyze.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    RepasswordComponent,
    ClientManagerComponent,
    ProjectManagerComponent,
    FileManagerComponent,
    FinanceManagerComponent,
    PersonnelManagerComponent,
    ClientInfoComponent,
    ClientServeComponent,
    ClientReserveComponent,
    ClientDevelopComponent,
    ClientTradeComponent,
    ClientAnalyzeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
