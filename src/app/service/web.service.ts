import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WebService {

  theme = 'light'

  constructor() { }

  setTheme(theme:string) {
    this.theme = theme
  }

  getTheme() {
    return this.theme
  }
}
