import { WebService } from './../service/web.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  url = ''
  theme = ''

  constructor(
    private router: Router,
    private webService: WebService
  ) { }

  ngOnInit(): void {
  }

  menuClass(url:string) {
    if (url == this.url) {
      return 'active'
    }

    else {
      return ''
    }
  }

  sidebarToggle() {
    const sidebarNavWrapper = document.querySelector(".sidebar-nav-wrapper");
    const mainWrapper = document.querySelector(".main-wrapper");
    sidebarNavWrapper?.classList.toggle("active")
    mainWrapper?.classList.toggle("active")
  }

  themeSwitcher(OC:string) {
    const optionBox = document.querySelector(".option-box");
    const optionOverlay = document.querySelector(".option-overlay");
    if (OC == "open") {
      optionBox?.classList.add("show")
      optionOverlay?.classList.add("show")
    }
    else if (OC == "close") {
      optionBox?.classList.remove("show")
      optionOverlay?.classList.remove("show")
    }
  }

  layoutChange(LR:string) {
    const leftSidebarButton = document.querySelector(".leftSidebarButton");
    const rightSidebarButton = document.querySelector(".rightSidebarButton");

    if (LR == "left") {
      document.body.classList.remove("rightSidebar")
      leftSidebarButton?.classList.add("active")
      rightSidebarButton?.classList.remove("active")
    }

    else if (LR == "right") {
      document.body.classList.add("rightSidebar")
      rightSidebarButton?.classList.add("active")
      leftSidebarButton?.classList.remove("active")
    }
  }

  themeChange(color:string) {
    const lightThemeButton = document.querySelector(".lightThemeButton");
    const darkThemeButton = document.querySelector(".darkThemeButton");

    if (color == "light") {
      this.theme = ''
      document.body.classList.remove("darkTheme");
      lightThemeButton?.classList.add("active")
      darkThemeButton?.classList.remove("active")
    }

    if (color == "dark") {
      this.theme = 'darkTheme'
      document.body.classList.add("darkTheme");
      darkThemeButton?.classList.add("active")
      lightThemeButton?.classList.remove("active")
    }
  }

  menuActive(url:string) {
    this.url = url
  }

  getTheme() {
    if (this.theme == 'darkTheme') {
      return 'darkTheme'
    }
    else {
      return ''
    }
  }
}
