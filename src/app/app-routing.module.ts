import { ClientAnalyzeComponent } from './body/client-manager/client-analyze/client-analyze.component';
import { ClientDevelopComponent } from './body/client-manager/client-develop/client-develop.component';
import { ClientTradeComponent } from './body/client-manager/client-trade/client-trade.component';
import { ClientReserveComponent } from './body/client-manager/client-reserve/client-reserve.component';
import { ClientServeComponent } from './body/client-manager/client-serve/client-serve.component';
import { ClientInfoComponent } from './body/client-manager/client-info/client-info.component';
import { ProjectManagerComponent } from './body/project-manager/project-manager.component';
import { FileManagerComponent } from './body/file-manager/file-manager.component';
import { RepasswordComponent } from './header/repassword/repassword.component';
import { RegisterComponent } from './header/register/register.component';
import { LoginComponent } from './header/login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    title: 'Mu | 首頁',
    component: LoginComponent
  },
  {
    path: 'login',
    title: 'Mu | 會員登入',
    component: LoginComponent
  },
  {
    path: 'register',
    title: 'Mu | 會員註冊',
    component: RegisterComponent
  },
  {
    path: 'resetpassword',
    title: 'Mu | 重設密碼',
    component: RepasswordComponent
  },
  {
    path: 'client-info',
    title: 'Mu | 客戶資訊',
    component: ClientInfoComponent
  },
  {
    path: 'client-serve',
    title: 'Mu | 服務紀錄',
    component: ClientServeComponent
  },
  {
    path: 'client-reserve',
    title: 'Mu | 預約紀錄',
    component: ClientReserveComponent
  },
  {
    path: 'client-trade',
    title: 'Mu | 交易紀錄',
    component: ClientTradeComponent
  },
  {
    path: 'client-develop',
    title: 'Mu | 客戶開發',
    component: ClientDevelopComponent
  },
  {
    path: 'client-analyze',
    title: 'Mu | 客戶分析',
    component: ClientAnalyzeComponent
  },
  {
    path: 'project-manager',
    title: 'Mu | 文件管理',
    component: ProjectManagerComponent
  },
  {
    path: 'file-manager',
    title: 'Mu | 文件管理',
    component: FileManagerComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
